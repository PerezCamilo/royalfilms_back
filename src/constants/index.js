export const HOST = 'http://localhost:19500/';
export const VERSION = 'api/v1/';

const APP_URL = {
    // ROUTES FILMS
    CREATE_FILM: `${HOST}${VERSION}admin/createFilms`,
}

export default APP_URL