import express from "express";
import colors from 'colors';
import Logger from './helpers/Logger';
import expressLoader from './loaders/express'
import { name, port } from './config';

async function startServer() {
    const app = express();
    await expressLoader(app)
    app.listen(port, () => {
        Logger.info(`${colors.yellow(
            '########################################################',
        )}
    🛡️  ${colors.green(
            `Server ${colors.magenta(name)} listening on port:`,
        )} ${colors.magenta(port)} 🛡️
${colors.yellow('########################################################')}`);
    }).on('error', (e) => Logger.error('error in server.listen', e));
}

startServer()
    .then(() => Logger.info(colors.green('done ✌️')))
    .catch((error) =>
        Logger.error(colors.magenta('error when starting the api'), error),
    );