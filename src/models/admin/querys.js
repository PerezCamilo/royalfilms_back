// QUERY FILMS
export const createFilm = `INSERT INTO films (name, languages, clasification, duration, release_date, trailer_url, synopsis, director_film, distribution_film) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`;
export const listLanguages = `SELECT * FROM parameters WHERE aux = 1`;
export const listClasification = `SELECT * FROM parameters WHERE aux = 2`;
export const updateFilm = `UPDATE films SET 
                                name = $1, 
                                languages = $2, 
                                clasification = $3,
                                duration = $4,
                                release_date = $5,
                                trailer_url = $6,
                                synopsis = $7,
                                director_film = $8,
                                distribution_film = $9
                                WHERE id = $10`;
export const deleteFilm = `DELETE FROM films WHERE id = $1`;
export const listFilms = `SELECT * FROM films`;