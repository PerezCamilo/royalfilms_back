import { Router } from "express";
import adminRoutes from "./admin";

const router = Router();

export default function () {
    router.use('/admin', adminRoutes());
    return router;
}
