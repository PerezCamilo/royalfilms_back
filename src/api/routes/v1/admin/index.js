import { Router } from 'express';
const adminController = require('../../../../controllers/adminController');

const router = Router();

export default () => {
    router.post('/createFilms', adminController.createFilms);
    router.get('/listFilms', adminController.listFilms);
    router.get('/listLanguages', adminController.listLanguages);
    router.get('/listClasification', adminController.listClasification);
    router.post('/updateFilm', adminController.updateFilm);
    router.post('/deleteFilm', adminController.deleteFilm);
    return router;
};
