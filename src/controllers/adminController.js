import {
  SuccessResponse,
  InternalError,
  BadRequestError,
} from '../helpers/api.response';
import { pool } from "../loaders/pgTool";
import * as querys from "../models/admin/querys";

// CREATE FILM
export const createFilms = async (req, res) => {
  const { nameFilm, language, clasification, duration, release_date, trailer_url, synopsis, director, distribution } = req.body;
  try {
    if (nameFilm === '' || language === '' || clasification === '' || duration === '' || release_date === '' || trailer_url === '' || synopsis === '' || director === '' || distribution === '') {
      return BadRequestError(res, 'All fields are required', false);
    } else {
      const listParameter = await pool.query(querys.createFilm, [nameFilm, language, clasification, duration, release_date, trailer_url, synopsis, director, distribution]);

      if (listParameter) {
        return SuccessResponse(res, 'Registered Successfully', true);
      } else {
        return BadRequestError(res, 'Registration failure', false);
      }
    }
  } catch (error) {
    console.log(error);
    InternalError(res);
  }
};

// LIST LANGUAGES
export const listLanguages = async (req, res) => {
  try {
    const listLanguages = await pool.query(querys.listLanguages);
    if (listLanguages.rows.length > 0) {
      return SuccessResponse(res, 'Languages list', true, { data: listLanguages.rows });
    } else {
      return BadRequestError(res, 'Empty langauges', false);
    }
  } catch (error) {
    console.log(error);
    InternalError(res);
  }
};

// LIST CLASIFICATION
export const listClasification = async (req, res) => {
  try {
    const listClasification = await pool.query(querys.listClasification);
    if (listClasification.rows.length > 0) {
      return SuccessResponse(res, 'Clasification list', true, { data: listClasification.rows });
    } else {
      return BadRequestError(res, 'Empty clasification', false);
    }
  } catch (error) {
    console.log(error);
    InternalError(res);
  }
};

// UPDATE FILM
export const updateFilm = async (req, res) => {
  const { nameFilm, language, clasification, duration, release_date, trailer_url, synopsis, director, distribution, id } = req.body;
  try {
    if (nameFilm === '' || language === '' || clasification === '' || duration === '' || release_date === '' || trailer_url === '' || synopsis === '' || director === '' || distribution === '') {
      return BadRequestError(res, 'All fields are required', false);
    } else {
      const updateFilm = await pool.query(querys.updateFilm, [nameFilm, language, clasification, duration, release_date, trailer_url, synopsis, director, distribution, id]);
      if (updateFilm) {
        return SuccessResponse(res, 'Film updated successfully', true);
      } else {
        return BadRequestError(res, 'Updated failure', false);
      }
    }
  } catch (error) {
    console.log(error);
    InternalError(res);
  }
};

//DELETE FILM
export const deleteFilm = async (req, res) => {
  const { id } = req.body;
  try {
    const deleteFilm = await pool.query(querys.deleteFilm, [id]);
    if (deleteFilm) {
      return SuccessResponse(res, 'Film delete successfully', true);
    } else {
      return BadRequestError(res, 'Deleted failure', false);
    }
  } catch (error) {
    console.log(error);
    InternalError(res);
  }
};

// LIST FILMS
export const listFilms = async (req, res) => {
  try {
    const listFilms = await pool.query(querys.listFilms);
    if (listFilms.rows.length > 0) {
      return SuccessResponse(res, 'Films list', true, { data: listFilms.rows });
    } else {
      return BadRequestError(res, 'Empty films', false);
    }
  } catch (error) {
    console.log(error);
    InternalError(res);
  }
};